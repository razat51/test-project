namespace movie.Models;

public class film {
    public string? Title { get; set; }
    public double Price { get; set; }

    public film() {
        Title = "";
        Price = 0;  
    }
}