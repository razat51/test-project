namespace helloApp  // Note: actual namespace depends on the project name.
{
    public class helloClass
    {
        public static string helloWorld() {
            return "Hello World!";
        }
    }
    
    public class HomeModel {
        public string Message() {
            return "Hello from Model";
        }
    }
}