﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using mvc.Models;
using movie.Models;
using helloApp;

namespace mvc.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    // void AssertIsNotNull([NotNull] object? nullableReference) {
        // if(nullableReference == null) {
           
        // }
   //  }

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult Privacy()
    {
        try {
            HomeModel m = new HomeModel();
            return View(m);
        }
        catch {
            throw new ArgumentNullException();
        }    
    }
    
    public IActionResult Model_Controller_View() {
        ViewBag.Title = "Model -> Controller -> View Page";
        film MoM = new film();
        MoM.Title =  "Dr. Strange";
        MoM.Price = 9.99;

        var list = new List<string> {
            "This statement is passed from Controller -> View",
            helloClass.helloWorld(),
            "^ The above statement is passed from Model -> Controller -> View"
        };
        
        ViewBag.mainBody = list;
        ViewBag.films = MoM;
        return View();
    }

    public IActionResult Model_View() {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}