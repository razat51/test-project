# ASP.NET MVC

[Back to main home page](../)

MVC is a design pattern used to decouple user-interface (view), data (model), and application logic (controller). This pattern helps to achieve separation of concerns.

Below is an MVC program

* <a href="./Programs/1.mvc" target="_blank"> Program 1</a> provides examples on how to transfer data from:
    * M->C-V 
    * M -> V
    
### Resources
- [Microsofts ASP.NET MVC Pattern](https://dotnet.microsoft.com/en-us/apps/aspnet/mvc)
- [General Resource for C# Data Passing](https://www.c-sharpcorner.com/UploadFile/abhikumarvatsa/various-ways-to-pass-data-from-controller-to-view-in-mvc/)
- [Resource for C# Data Passing from Controller -> View](https://www.c-sharpcorner.com/article/asp-net-mvc-passing-data-from-controller-to-view/#:~:text=from%20the%20ViewData.-,The%20other%20way%20of%20passing%20the%20data%20from%20Controller%20to,model%20class%20in%20return%20view.&text=Import%20the%20binding%20object%20of,access%20the%20properties%20by%20%40Model)