﻿using System;

namespace practice // Note: actual namespace depends on the project name.
{
    internal class Program
    {
        static int[] genericArr( int number, int length ) {
            int[] arr = new int [length];
            for (int i = 0; i < length; i++ ) {
                arr[i] = number;
            }
            return arr;
        }

        static void printArr( int[] arr ) {
            foreach(var item in arr) {
                Console.WriteLine(item.ToString());
            }
        }

        static void Main(string[] args)
        {
            printArr ( genericArr ( 2, 5) );

            // array of 4 elements
            int[] arr = new int[4];
            arr[0] = 500;
            arr[1] = 600;
            arr[2] = 700;
            arr[3] = 800;
            printArr (arr);

            string[] arr1 = { "Ava", "Emma", "Olivia" };
            string[] arr2 = { "Olivia", "Sophia", "Emma" };

            var varArr1 = new string[] { "Ava", "Emma", "Olivia" };
            var varArr2 = new string[] { "Olivia", "Sophia", "Emma" };

        }
    }
}