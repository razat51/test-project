﻿// See https://aka.ms/new-console-template for more information
using System.Collections.Generic;
using System.Linq;

static void printList( List<string> list ) {
    foreach(var item in list) {
        Console.WriteLine(item.ToString());
    }
}

// List with default capacity
List<int> list1 = new List<int>();

// List with capacity = 5
List<string> authors = new List<string>(5);

// Adding elements of an array to a list.
string[] animals = { "Cow", "Camel", "Elephant" };
List<string> animalsList = new List<string>(animals);
animalsList.Add("Zebra");

string[] arr1 = { "Ava", "Emma", "Olivia" };
animalsList.AddRange(arr1);
printList(animalsList);

// USEFUL RESOURCES
/* https://www.codegrepper.com/code-examples/csharp/get+list+length+c%23 */
// https://www.geeksforgeeks.org/c-sharp-how-to-check-whether-a-list-contains-a-specified-element/
// https://www.codegrepper.com/code-examples/csharp/get+list+length+c%23