﻿// See https://aka.ms/new-console-template for more information
using System;

namespace practice // Note: actual namespace depends on the project name.
{
    internal class Program
    {
        static int[] genericArr( int number, int length ) {
            int[] arr = new int [length];
            for (int i = 0; i < length; i++ ) {
                arr[i] = number;
            }
            return arr;
        }

        static void printArr( int[] arr ) {
            foreach(var item in arr) {
                Console.WriteLine(item.ToString());
            }
        }

        public List<int> genericList( int item, int length ) {
            List<int> numberList = new List<int>();
            for ( int i = 0; i < length; i++ ) {
                numberList.Add(item);
            }
            return numberList;
        }

        static void printList( List<int> list ) {
            foreach(var item in list) {
                Console.WriteLine(item.ToString());
            }
        }

        // Tuples!
        static Tuple<int, string> basicTuple () {
            var person = Tuple.Create(0, "turtle");
            return person;
        } 

        // https://stackoverflow.com/questions/60563145/loop-a-list-with-a-tuple-c-sharp
        
        static List<(int, string)> basicTupleList ( )  {  
            var tupleList = new List<(int, string)>() {
                (1, "cow"),
                (basicTuple().Item1, basicTuple().Item2),
                (5, "chickens"),
                (1, "airplane")
            };
            return tupleList;  
        }  

        // Calling the below function PUBLIC will cause an issue with the code! 
        static void printTupleList ( List<(int, string)> tupleList) {
            foreach (var currentTuple in tupleList) {
                Console.WriteLine( currentTuple.Item1.ToString() + " " + currentTuple.Item2 );
            }
        }

        static List<(int,string)> generateTupleListFromArr ( string[] arr ) {
            var nameList = new List<(int, string)>();
            for (int i = 0; i < arr.Length; i++ ) {
                nameList.Add ( (i, arr[i]) );
            }
            return nameList;
        }

        // string methods
        static string popFront( string word ) {
            return word.Substring( 1, word.Length - 1 );
        }

        static string popBack( string word ) {
            return word.Substring( 0, word.Length - 1 );
        }

        // https://www.educative.io/edpresso/how-to-insert-one-string-into-another-using-insert-in-c-sharp

        static void hellowWorld() {
            Console.WriteLine("Hello");
        }

        static void Main(string[] args)
        {
            // printArr ( genericArr ( 2, 5) );
            // printList ( genericList ( 1, 2) );
            // printTupleList ( basicTupleList() );
            
            // string[] names = { "Raza", "Stone" };
            // printTupleList ( generateTupleListFromArr( names ) );

            string name = "Raza";
            // name = popFront(name);
            name = popBack(name);
            Console.WriteLine ( name );
        }
    }
}