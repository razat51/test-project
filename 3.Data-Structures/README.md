# Data Structures Overview

[Back to main home page](../)

Here are some useful programs that go over data structures

* [Program 1](3-Programs/Program1.cs): Generic Helper Functions for Arrays, Lists and Tuples
* [Program 2](3-Programs/Program2.cs): Basic Array Manipulations
* [Program 3](3-Programs/Program3.cs): Basic List Manipulations

{% hint style="info" %}
**Good to know:** You can use subpages to cleanly nest a bunch of content in its own little 'home' in your table of contents. This lets you keep on top of long lists like recurring meeting notes without having to maintain a huge list of top-level pages.
{% endhint %}
