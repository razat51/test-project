﻿// See https://aka.ms/new-console-template for more information
using System;
namespace objects
{    
    public interface IAnimal {
        // Uncommenting the below line will crash compiler since interfaces shouldn't have fields/variables!
        // public String? name { get; set; }
        void animalSound(); // interface method (does not have a body)
    }

    class Pig : IAnimal {
        // The body of animalSound() is provided here
        public void animalSound() 
        {
            Console.WriteLine("The pig says: wee wee");
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Pig myPig = new Pig();
            myPig.animalSound();

            // Uncommenting below line will cause an error, as it should since interfaces shouldn't have objects!
            // IAnimal animal = new IAnimal();
        }
    }
}

// https://www.w3schools.com/cs/cs_interface.php
// C# does not support multiple class inheritance as it may lead to a problem where the same features gets inherited multiple times. 
// But the same functionality can be achieved using multiple interface implementation.