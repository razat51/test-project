﻿using System;
namespace objects // Note: actual namespace depends on the project name.
{
    public class Employee
    {       
        // https://stackoverflow.com/questions/67505347/non-nullable-property-must-contain-a-non-null-value-when-exiting-constructor-co
        public String? name { get; set; }
        public int? age { get; set; }

        // Default Constructor 
        public Employee() {  
        }

        // Constructor Overloading i.e. constructor with a different set of parameters.
        public Employee(String name, int Age) {  
            this.name = name;
            age = Age;
        }

        public void printAttributes() {
            Console.WriteLine(name + " " + age + " " );
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Employee raza = new Employee();
            raza.name = "Raza";
            raza.age = 27;
            Console.WriteLine(raza.name + " " + raza.age + " ");

            Employee newguy = new Employee ( "Carlos", 30 );
            newguy.printAttributes();
        }
    }
}