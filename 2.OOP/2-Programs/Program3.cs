﻿using System;
namespace MyTestConsoleApp
{
    public class Employee
    {
        public string? Name { get; set; }
        public int? Age { get; set; }
    }
    public class PermanentEmployee : Employee
    {
        public int? salary { get; set; }
        public PermanentEmployee(int salary)
        {
            this.salary = salary;
        }
    }
    public class ContractEmployee : Employee
    {
        public int hourlypay { get; set; }
        public ContractEmployee(int hourlypay)
        {
            this.hourlypay = hourlypay;
        }
    }
    public class Developer : Employee 
    {
        public string getOccupation() {
            return ("Developer");
        }
    }

    internal class Program { 
        static void Main(string[] args)
        {
            PermanentEmployee pe = new PermanentEmployee(5000);
            ContractEmployee ce = new ContractEmployee(15);
            Developer dev1 = new Developer();

            pe.Name = "Ram";
            pe.Age = 30;
            ce.Name = "John";
            ce.Age = 25;
            Console.WriteLine( "Name = {0}, Age = {1}, Salary = {2}", pe.Name, pe.Age, pe.salary );
            Console.WriteLine( "Name = {0}, Age = {1}, Hourly Pay = {2}", ce.Name, ce.Age, ce.hourlypay );
            
            dev1.Name = "Alex";
            Console.WriteLine ( "Name = {0}, Occupation = {1}", dev1.Name, dev1.getOccupation() );
        }
    }
}

// References:
// https://masterdotnet.com/csharptutorial/csharpinheritance/
// https://www.c-sharpcorner.com/article/overview-of-inheritance-in-c-sharp/
// https://www.w3schools.com/cs/cs_inheritance.php