﻿// C# program to illustrate encapsulation
// Encapsulation aka data-hiding is a principle of OOP that hides data in a class from itself or other classes
using System;
namespace MyTestConsoleApp
{
    public class class1 {
        private void validate() {
            Console.WriteLine("Asserting that card is valid....");
        }
        private void charge() {
            Console.WriteLine("Placing charge...");
        }
        private void confirmation() {
            Console.WriteLine("Approved!");
        }
        public void checkout() {
            validate();
            charge();
            confirmation();
        }
    }

    public class class2 {
        // public variable declared i.e. where Encapsulation DOESN'T work.
        public String? height { get; set; }

        // private variables declared so that these can ONLY be accessed by public methods of class
        private String studentName = "";
        private int studentAge = 0;
         
        // using accessors to get and set the value of studentName
        public String NameProperty {
            get {
                return studentName;    
            }
            set {
                studentName = value;
            }
        }
        
        // longer method
        public int getAge () {  
            return studentAge;    
        }
            
        public void setAge (int value) {
            studentAge = value;
        }
    }

    internal class Program { 
        static void Main(string[] args)
        {
            // Part 1: Encapsulation with methods
            class1 obj = new class1();
            obj.checkout();
            // Code won't work due to encapsulation!: obj4.validate();

            // PART 2: NO ENCAPSULATION
            class2 obj1 = new class2();
            obj1.height = "6ft";
            Console.WriteLine("\nObj 1\nHeight: " + obj1.height);

            // PART 3: Encapsulation
            class2 obj2 = new class2();
            obj2.NameProperty = "Ankita"; // calls set accessor of the property Name and pass "Ankita" as value of the standard field 'value'
            obj2.setAge ( 21 );
            Console.WriteLine("Obj 2\nName: " + obj2.NameProperty);
            // Due to encapsulation, deleting the below line will cause an error (as it should!):
            // Console.WriteLine("Age: " + obj2.studentAge);
            Console.WriteLine("Age: " + obj2.getAge() ); // The proper way to get the age is through the getAge method
        }
    }
}

// Reference: https://www.geeksforgeeks.org/c-sharp-encapsulation/