﻿using System;

namespace polymorphism
{
  public class Animal  // Base class (parent) 
  {
    public void animalSound()
    {
      Console.WriteLine("The animal makes a sound");
    }
    public virtual void sleep()
    {
      Console.WriteLine("The animal is sleeping");
    }
  }
  public class Pig : Animal  // Derived class (child) 
  {
    public new void animalSound()
    {
      Console.WriteLine("The pig says: wee wee");
    }
    public new void sleep()
    {
      Console.WriteLine("The pig is zzz");
    }
    public void bad() {
      Console.WriteLine("Calling this method from the parent class is bad. Otherwise calling from inherited class is okay.");
    }
  }

  class Dog : Animal  // Derived class (child) 
  {
    public new void animalSound()
    {
      Console.WriteLine("The dog says: bow wow");
    }
    public override void sleep()
    {
      Console.WriteLine("The dog is asleep");
    }
  }

  public class Program
  {
    static void Main(string[] args)
    {
      Animal myAnimal = new Animal();  // Create a Animal object
      Pig neighborsPig = new Pig(); // NO POLYMORPHISM
      Animal myPig = new Pig();  // Create a Pig polymorphic object
      Animal myDog = new Dog();  // Create a Dog polymorphic object 
      
      myAnimal.animalSound();
      myAnimal.sleep();
      Console.WriteLine();

      Console.WriteLine("This neighbor pig object is not polymorphic");
      neighborsPig.animalSound();
      neighborsPig.sleep();
      neighborsPig.bad(); // bad() works since we're using an inherited object.
      Console.WriteLine();

      myPig.animalSound();
      Console.WriteLine("Pig Object is of type: " + myPig.GetType());
      // myPig.bad(); // This line will not work. For a polymorphic object to use a child method, the parents class must define the method & the child class must override it.
      myPig.sleep();
      Console.WriteLine("");

      myDog.animalSound();
      Console.WriteLine("Dog Object is of type: " + myDog.GetType()); 
      myDog.sleep();
    }
  }
}

// REFERENCES:
// https://www.w3schools.com/cs/cs_polymorphism.php
// https://stackoverflow.com/questions/18279663/get-the-class-name-from-an-object-variable

/*
https://stackoverflow.com/questions/6308178/what-is-the-main-difference-between-inheritance-and-polymorphism
In Java, the two are closely related. This is because Java uses a technique for method invocation called "dynamic dispatch". If I have

public class A {
  public void draw() { ... }
  public void spin() { ... }
}

public class B extends A {
  public void draw() { ... }
  public void bad() { ... }
}

...

A testObject = new B();

testObject.draw(); // calls B's draw, polymorphic
testObject.spin(); // calls A's spin, inherited by B
testObject.bad(); // compiler error, you are manipulating this as an A
 */