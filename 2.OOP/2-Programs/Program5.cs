﻿// See https://aka.ms/new-console-template for more information
using System;
namespace objects // Note: actual namespace depends on the project name.
{
    abstract class AnimalAbstract {
        // Abstract method (does not have a body)
        public abstract void animalSound();
        public String? name { get; set; }
        
        // Regular method. NOTE: This won't work if you add it to an interface.
        public void sleep()
        {
            Console.WriteLine("Zzz");
        }
    }

    // Derived class (inherit from Animal)
    class Owl : AnimalAbstract {
        public override void animalSound()
        {
            // The body of animalSound() is provided here
            Console.WriteLine("The pig says: wee wee .... no wait its HOOT HOOT!");
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Owl myOwl = new Owl();  // Create a Pig object
            myOwl.animalSound();
            myOwl.sleep();

            // Uncommenting below line will cause an error, as it should!
            // AnimalAbstract animal = new AnimalAbstract();
        }
    }
}

// Example taken from: https://www.w3schools.com/cs/cs_abstract.php