# OOP Programs
[Back to main home page](../README.md)

This chapter goes over OOP. 

<ol>
    <li>
        <a href="./2-Programs/Program1.cs" target="_blank">
        Program 1</a>: Introduction to Classes & Objects 
    </li>
    <li>
        <a href="./2-Programs/Program2.cs" target="_blank">
        Program 2</a>: Encapsulation
    </li>
    <li>
        <a href="./2-Programs/Program3.cs" target="_blank">
        Program 3</a>: Introduction to Inheritance 
    </li>
    <li>
        <a href="./2-Programs/Program4.cs" target="_blank">
        Program 4</a>: Basics example of interfaces
    </li>
    <li>
        <a href="./2-Programs/Program5.cs" target="_blank">
        Program 5</a>: Basic example of abstract classes 
    </li>
    <li>
        <a href="./2-Programs/Program6.cs" target="_blank">
        Program 6</a>: Polymorphism 
    </li>
</ol>

## OOP Principles
<strong> OOP </strong> is a programming model that organizes software design around data, or objects, rather than on functions and logic.
In OOP:

* <strong> Objects </strong> can be considered as an instance of a class. 
* <strong> Classes </strong> can be considered as a user defined data type. 

The 4 main principles of OOP are:
<ol>
    <li> Encapsulation i.e. "Data Hiding"  </li>
    <li> 
    Abstraction: The process of hiding certain details AND showing only high level essential information to the user.
    <ul><li>Abstraction can be thought of as a natural extension of encapsulation but with the fact that each object should only expose high-level mechanisms for using it e.g. you shouldn't know whats "under the hood" to "start a car".</li></ul>
    </li>
    <li> Inheritance defines a derived class that extends or inherits fields and methods from another class. <ul><li> Child class has an "is a" relationship with the parent class. </li></ul>
    </li>
    <li> Polymorphism allows a child class to use its parents original methods and yet still allow the child class to keep its own methods.</li>
</ol>

Reference: <br>
[OOP ELI5](https://www.freecodecamp.org/news/object-oriented-programming-concepts-21bb035f7260/)
