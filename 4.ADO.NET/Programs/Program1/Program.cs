﻿// See https://aka.ms/new-console-template for more information
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace connecttoSSMS {
  class Program {
    static void createRow(SqlConnection conn) {
      //create a new SQL Query using StringBuilder
      StringBuilder strBuilder = new StringBuilder();
      strBuilder.Append("INSERT INTO PERSON (PersonID, FirstName, City) VALUES ");
      strBuilder.Append("(2, N'Raza', N'LA');");
      // strBuilder.Append("(N'Ronak', N'ronak@gmail.com', N'Class X') ");

      string sqlQuery = strBuilder.ToString();
      using(SqlCommand command = new SqlCommand(sqlQuery, conn)) //pass SQL query created above and connection
      {
        command.ExecuteNonQuery(); //execute the Query
        Console.WriteLine("Query Executed.");
      }
    }

    // Alternate way to add Row
    public static void addRow(SqlConnection conn) {
      //  https://csharp-station.com/Tutorial/AdoDotNet/Lesson03   
      SqlCommand cmd = new SqlCommand(); // Creating instance of SqlCommand  
      // prepare command string
      string insertString = @" 
      INSERT INTO PERSON
        (PersonID, FirstName, City)
      VALUES(10, 'Raza', 'USA')
      ";
      cmd.Connection = conn; // set the connection to instance of SqlCommand  
      cmd.CommandText = insertString; // set the sql command ( Statement )  
      cmd.ExecuteNonQuery(); //execute the Query
      Console.WriteLine("Query Executed.");
    }

    public static void performStatement(SqlConnection conn, string updateString) {
      //  https://csharp-station.com/Tutorial/AdoDotNet/Lesson03 
      SqlCommand cmd = new SqlCommand(); // Creating instance of SqlCommand 
      cmd.Connection = conn; // set the connection to instance of SqlCommand  
      cmd.CommandText = updateString; // set the sql command ( Statement )  
      cmd.ExecuteNonQuery(); //execute the Query
      Console.WriteLine("Query Executed.");
    }

    public static void updateRow(SqlConnection conn) {
      string updateString = @" 
      UPDATE PERSON
      set city = 'NY'
      where city = 'LA'
      ";
      performStatement(conn, updateString);
    }

    public static void deleteRow(SqlConnection conn) {
      string city = "NY";
      string deleteString = @" DELETE from PERSON where city = '" + city + "'";
      performStatement(conn, deleteString);
    }

    public static void addPersonProcedure(SqlConnection conn) {
      string procedureString = @" 
      EXEC addnicks;
      ";
      performStatement(conn, procedureString);
    }

    public static void deletePersonProcedure(SqlConnection conn) {
      string fname = "Raza";
      string procedureString = @" 
      EXEC deleteperson @Fname = '" + fname + "'";
      performStatement(conn, procedureString);
    }

    public static void readData(SqlConnection conn) {
      // 1. Instantiate a new command with a query and connection
      SqlCommand cmd = new SqlCommand("SELECT FirstName, City from PERSON", conn);
      // 2. Call Execute reader to get query results
      SqlDataReader rdr = cmd.ExecuteReader();
      while (rdr.Read()) {
        Console.WriteLine("Name: {0}, City: {1}", rdr[0], rdr[1]);
      }

      // close the reader
      if (rdr != null) {
        rdr.Close();
      }
    }

    public static void executeQuery(SqlConnection conn) {
      // Functions
      // addRow(conn);
      // createRow(conn);
      readData(conn);
      // updateRow(conn);
      // deleteRow(conn);
      // addPersonProcedure(conn);
      // deletePersonProcedure(conn);
    }

    public static string setInformation() {
      var datasource = @"HOLLIDAY-2021"; //your server
      var database = "razadb2"; //your database name
      var username = "raza"; //username of server to connect
      var password = "tahir"; //password

      //your connection string 
      string connString = @"Data Source=" + datasource + ";Initial Catalog=" +
        database + ";Persist Security Info=True;User ID=" + username + ";Password=" + password;

      return connString;
    }

    public static void executeInstance(String connString) {
      //create instanace of database connection
      SqlConnection conn = new SqlConnection(connString);

      try {
        Console.WriteLine("Opening Connection ...");

        //open connection
        conn.Open();
        Console.WriteLine("Connection successful!");

        // execute query
        Console.WriteLine("...");
        executeQuery(conn);
        // readData(conn); // this works as well
        Console.WriteLine("...\nQuery executed!");
        return;
      } catch (Exception e) {
        Console.WriteLine("Error: " + e.Message);
      }

      Console.Read();
      conn.Close(); // Close the connection  
    }

    static void Main(string[] args) {
      Console.WriteLine("Hello World!");
      executeInstance(setInformation());
      Console.WriteLine("Goodbye World!");
    }
  }
}

// https://docs.microsoft.com/en-us/sql/connect/ado-net/step-3-connect-sql-ado-net?view=sql-server-ver16
// https://docs.microsoft.com/en-us/dotnet/framework/data/adonet/ado-net-code-examples#sqlclient