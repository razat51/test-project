# ASP.NET ADO.NET

[Back to main home page](../)

In short, ADO.NET lets your program get access to data sources such as SQL Server and XML. 

Below is an example of a .NET program that accesses a MSSQL server. 

* <a href="./Programs/Program1/Program.cs" target="_blank"> Program 1</a> 

Some things to note:
* The example uses a SqlConnection object. [Resource](https://docs.microsoft.com/en-us/dotnet/framework/data/adonet/connecting-to-a-data-source)
* If you wish to copy/paste the code from the Program1.cs file into your own local program, you will need to:
    * Most importantly, add a NUGET package through the Nuget Package Manager called 'System.Data.SqlClient'. Without the package, you will see the error: 

    ``` 
    The type name 'SqlConnection' could not be found in the namespace 'System.Data.SqlClient'.
    ```

    * Make sure the tables in the database are configured properly.
    * Add procedures to the database if you plan to use the function