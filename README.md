# Reviewing ASP.NET

Hello! Here are some convenient programs and notes about ASP.NET that are stored in one place [here](https://raza-tahir.gitbook.io/untitled-1/). Indexable, searchable and deep-linkable (yes, that's a word).
<br/>

##  Table of Contents
1. [Get Started](./1.Getting-Started/getting-started.md)
2. [OOP](./2.OOP/README.md)
3. [Data Structures](./3.Data-Structures/README.md)
4. [ADO.NET](4.ADO.NET/Readme.md)
5. [MVC](./5.MVC/Readme.md)
6. [TBD](#)

### How it works
Each "chapter" reflects a different folder in the root directory of the repo. Within each folder you can expect to find a README file as well as a subfolder that holds all the relevant programs for that chapter.

### How this project operates
This project consists of a series of markdown files that have been pushed to a Gitlab Repo. Additionally, this project is integrated with GitBooks so that a static site can be generated from the markdown files. You can read more about [how to sync git with Gitbook](https://docs.gitbook.com/integrations/git-sync). 

This method is great since the content from the Gitlab repo automatically gets synchronized with Gitbooks and can be done by simply by pushing any local commits to Gitlab. Here is [a good video that shows how the integration can be set up](https://www.youtube.com/watch?v=d4s0Ks0e-tA&t=1s).