﻿// See https://aka.ms/new-console-template for more information
using function1Namespace;
Class1.printHello();

// Without the using statement. Note: its case sensitive!
function2Namespace.Class2.printHi();