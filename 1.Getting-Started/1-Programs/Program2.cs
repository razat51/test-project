// See https://aka.ms/new-console-template for more information
using System;

namespace ConsoleApp  // Note: actual namespace depends on the project name.
{
    internal class Program
    {
        static void helloWorld() {
            Console.WriteLine("Hello World!");
        }

        static void Main(string[] args) {
            helloWorld();
            Console.WriteLine("I hope you are well!");
        }
    }
}