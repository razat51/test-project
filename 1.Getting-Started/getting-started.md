# Getting Started
[Back to main home page](../README.md)

Disclaimer: To create a new console app, run the command:

> dotnet new console -n newApp

Here is a list of simple default console programs to help get you started.

<ul>
    <li>
        <a href="./1-Programs/Program1.cs" target="_blank">
        Program 1</a>: Hello World Program with top level statements 
    </li>
    <li>
        <a href="./1-Programs/Program2.cs" target="_blank">
        Program 2</a>: Hello World Program using the older template
    </li>
    <li>
        <a href="./1-Programs/Program3">
        Program 3</a>: Call functions from other namespaces
    </li>
</ul>